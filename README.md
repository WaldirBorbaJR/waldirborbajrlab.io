## 👋 Waldir Borba Junior 

Software Backend En·gi·neer ⬘ from Brazil to the World and now trying to learn Android Mobile development.

Owner & Software Engineer at my own company B+ Technology focus on GoLang and Computer Vision & Machine Learning.

I love making open-source projects and cool products, sharing with others, and enjoying the feeling of creativity.

<p align="left"> <img src="https://komarev.com/ghpvc/?username=waldirborbajr&label=Profile%20views&color=0e75b6&style=flat" alt="waldirborbajr" /> </p>

<img src="https://user-images.githubusercontent.com/40029512/212558846-bc92f10b-0051-4e9a-8cd2-309f5e61e319.gif" width="600px">

<!-- Your title -->

<details>
  <summary><b>Holopin</b></summary><br>

[![@waldirborbajr's Holopin board](https://holopin.io/api/user/board?user=waldirborbajr)](https://holopin.io/@waldirborbajr)

</details>

<details>
  <summary><b>Skills</b></summary><br>

[![My Skills](https://skillicons.dev/icons?i=go,bash,docker,git,gitlab,github,githubactions,linux,bsd,md,postgresql,mongodb,mysql,sqlite,py,neovim,arduino,raspberrypi,rust,redis,latex&perline=10)](https://skillicons.dev)
</details>

<details>
  <summary><b>Connect with me</b></summary><br>

[<img alt="Gitlab" src="https://skillicons.dev/icons?i=gitlab"/>](https://gitlab.com/waldirborbajr)
[<img alt="Github" src="https://skillicons.dev/icons?i=github"/>](https://github.com/waldirborbajr)
[<img alt="LinkedIn" src="https://skillicons.dev/icons?i=linkedin"/>](https://www.linkedin.com/in/waldirborbajr/)
[<img alt="Instagram" src="https://skillicons.dev/icons?i=instagram"/>](https://instagram.com/waldirborbajr)
[<img alt="Twitter" src="https://skillicons.dev/icons?i=twitter"/>](https://twitter.com/waldirborbajr)
</details>
  
<details>
  <summary><b>💖 Support the Project</b></summary><br>

Thank you so much already for using my projects! If you want to go a step further and support my open source work, buy me a coffee:

<div>
  <a href="https://ko-fi.com/waldirborbajunior">
    <img align="center" height="49px" src="https://cdn.ko-fi.com/cdn/kofi1.png?v=3" />
  </a>
  <a href="https://www.buymeacoffee.com/waldirborbajr">
    <img align="center" height="49px" src="https://www.vectorlogo.zone/logos/buymeacoffee/buymeacoffee-official.svg" />
  </a>
</div>
To support the project directly, feel free to open issues, or contribute with a pull request!
</details>
